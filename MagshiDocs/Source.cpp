#pragma comment(lib, "ws2_32.lib")
#include "Server.h"
#include "WSAInitializer.h"
#include <exception>
#include <iostream>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server server;
		server.serve(6587);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	
	return 0;
}