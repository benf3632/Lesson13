#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <iterator>
#include <vector>
#include <deque>
#include <exception>
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>


#define TYPE_CODE_SIZE 3
#define FILE_CONTENT_AFTER 8
#define MAXIMUM_FILE_SIZE_IN_BYTES 5
#define NEXT_INQUEUE 1


using namespace std;



class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	
private:
	//private methods
	void accept();
	void clientHandler(SOCKET sc);
	void messagesHandler();
	void sendUpdateMessageToAllClients();
	int findIndexBySocket(SOCKET sc);
	void updateQueue();
	void disconnectClient(SOCKET sc);
	void writeToFile();

	//data struct
	SOCKET _serverSocket;
	deque<pair<string, SOCKET>> _users;
	vector<thread> _usersThread;
	deque<string> _msgQueue;

	//other
	fstream _dataFile;
	string _fileContent;
	bool _openWorker;

	//lockers
	mutex _muMsgQueue;
	mutex _muFileCon;
	mutex _muUsers;
	condition_variable _cond;
	condition_variable _condFileWrite;
};

