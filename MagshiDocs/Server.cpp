#include "Server.h"



Server::Server()
{
	//starts listening socket
	std::cout << "Starting..." << std::endl;
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	//opens data file
	_dataFile.open("data.txt",ios::in);
	if (!_dataFile.is_open()) //checks if the file exists
	{
		_dataFile.open("data.txt", ios::out); //creates new file
	}
	_fileContent.assign(istreambuf_iterator<char>(_dataFile), istreambuf_iterator<char>()); //reads all the file

	_dataFile.close(); //close it for the writeToFile thread

}


Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}

	//saves the data content to a file
	if (_dataFile.is_open())
	{
		_dataFile.close();
	}
	_dataFile.open("data.txt", ios::out | ios::trunc);
	_dataFile << _fileContent;
	//closes data file
	_dataFile.close();

	//clears thread vector
	_usersThread.clear();
}

/*
Bindes the server socket to a port and start listening
*/
void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	std::cout << "Binding..." << std::endl;
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	std::cout << "Listening..." << std::endl;
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	
	std::cout << "Accepiting clients..." << std::endl;
	while (true)
	{
		accept();
	}
}

/*
Accepts new client and adds them to the server
*/
void Server::accept()
{
	SOCKET client_sock = ::accept(_serverSocket, NULL, NULL);

	if (!_openWorker) //checks if the messages handler thread was started already
	{
		thread a(&Server::messagesHandler, this);
		a.detach();
		_openWorker = true;
	}

	if (client_sock == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	//locks the data bases
	lock_guard<mutex> fileLck(_muFileCon);
	lock_guard<mutex> usersLck(_muUsers);
	
	//gets his userName
	Helper::getMessageTypeCode(client_sock);
	int userSize = Helper::getIntPartFromSocket(client_sock, 2);
	string userName = Helper::getStringPartFromSocket(client_sock, userSize);
	
	_users.push_back(pair<string, SOCKET>(userName, client_sock)); //inserts client prop to db

	//send first update message to the client
	try
	{
		if (_users.size() == 1) //if he is first client
		{
			Helper::sendUpdateMessageToClient(client_sock, _fileContent, userName, "", 1); //sends as first
		}
		else
		{
			Helper::sendUpdateMessageToClient(client_sock, _fileContent, _users.front().first, _users[1].first, findIndexBySocket(client_sock) + 1);
		}
	}
	catch (...) {}

	//starts client thread
	_usersThread.push_back(thread(&Server::clientHandler, this, client_sock));
	_usersThread.back().detach();	
}

/*
inserts messages recived from clients to the message queue
*/
void Server::clientHandler(SOCKET sc)
{
	bool stop = false;
	int index = 0;
	while (!stop)
	{
		try {
			int typeCode = Helper::getMessageTypeCode(sc); //gets type code
			switch (typeCode)
			{
			case MT_CLIENT_FINISH:
			case MT_CLIENT_UPDATE:
			{
				string fileSize = Helper::getStringPartFromSocket(sc, MAXIMUM_FILE_SIZE_IN_BYTES); //gets file size
				string fileCon = Helper::getStringPartFromSocket(sc, stoi(fileSize)); //gets file content
				lock_guard<mutex> lck(_muMsgQueue); //locks the message queue
				_msgQueue.push_back(to_string(typeCode) + fileSize + fileCon); //inserts the message to the queue
				break;
			}
			case MT_CLIENT_EXIT:
			{
				lock_guard<mutex> lck(_muMsgQueue); //locks the message queue
				_msgQueue.push_back(to_string(typeCode) + to_string(sc)); //inserts the message to the queue
				stop = true; //stops the thread
				break;
			}
			}
			_cond.notify_one(); //wakes message handler
		}
		catch (...) 
		{
			lock_guard<mutex> lck(_muMsgQueue); //locks the message queue
			_msgQueue.push_back(to_string(MT_CLIENT_EXIT) + to_string(sc)); //inserts the message to the queue
			stop = true; //stops the thread
			break;
		}
	}
	
}

/*
Handles message the clients sent
*/
void Server::messagesHandler()
{
	unique_lock<mutex> fileConLck(_muFileCon);
	fileConLck.unlock();

	int contentSize = 0;

	//start writing to file thraed
	thread writeToFile(&Server::writeToFile, this);
	writeToFile.detach();

	while (true)
	{
		unique_lock<mutex> lck(_muMsgQueue);
		_cond.wait(lck, [&]() { return !_msgQueue.empty(); }); //wait until there is messages

		while (!_msgQueue.empty())
		{
			int typeCode = stoi(_msgQueue.front().substr(0, TYPE_CODE_SIZE)); //get the typcode of the message
			switch (typeCode)
			{
			case MT_CLIENT_UPDATE:
			{
				fileConLck.lock();
				contentSize = stoi(_msgQueue.front().substr(TYPE_CODE_SIZE, MAXIMUM_FILE_SIZE_IN_BYTES));
				if ( contentSize == 0) //if the content is empty
				{
					_fileContent = "";
				}
				else
				{
					_fileContent = _msgQueue.front().substr(FILE_CONTENT_AFTER, contentSize); //save the content

				}
				fileConLck.unlock();
				_condFileWrite.notify_one(); //wakes thread that writes to file
				sendUpdateMessageToAllClients(); //updates clients
				break;
			}
			case MT_CLIENT_FINISH:
				fileConLck.lock();
				contentSize = stoi(_msgQueue.front().substr(TYPE_CODE_SIZE, MAXIMUM_FILE_SIZE_IN_BYTES));
				if (contentSize == 0) //if the content is empty
				{
					_fileContent = "";
				}
				else
				{
					_fileContent = _msgQueue.front().substr(FILE_CONTENT_AFTER, contentSize); //save the content

				}
				fileConLck.unlock();
				_condFileWrite.notify_one();
				updateQueue();
				sendUpdateMessageToAllClients();
				break;
			case MT_CLIENT_EXIT:
				disconnectClient(stoi(_msgQueue.front().substr(TYPE_CODE_SIZE, _msgQueue.front().size())));
				sendUpdateMessageToAllClients();
				break;
			}
			_msgQueue.pop_front(); //pops from queue 
		}
		lck.unlock();
	}

}

/*
Sends an update message to all connected clients
*/
void Server::sendUpdateMessageToAllClients()
{
	for (int i = 0; i < _users.size(); i++)
	{

		lock_guard<mutex> fileConLck(_muFileCon);

		try 
		{
			if (_users.size() == 1)
			{
				Helper::sendUpdateMessageToClient(_users.front().second, _fileContent, _users.front().first, "", NEXT_INQUEUE);
			}
			else
			{

				Helper::sendUpdateMessageToClient(_users[i].second, _fileContent, _users.front().first, _users[NEXT_INQUEUE].first, findIndexBySocket(_users[i].second) + 1);
			}
		}
		catch (...) {}
	}
}

/*
Findes index of users by socket
*/
int Server::findIndexBySocket(SOCKET sc)
{
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i].second == sc) 
		{
			return i;
		}
	}
	return -1;
}

/*
Updates the queue (puts the first in queue the last in queue)
*/
void Server::updateQueue()
{
	lock_guard<mutex> lck(_muUsers);//locks users and write queue

	pair<string, SOCKET> temp(_users.front()); //saves the data
	_users.pop_front(); //pops from front
	_users.push_back(temp); //pushes to the back
}

/*
Disconnects a client from the server
*/
void Server::disconnectClient(SOCKET sc)
{
	unique_lock<mutex> lck(_muUsers);
	int index = findIndexBySocket(sc);

	closesocket(_users[index].second); //cloeses socket
	_users.erase(_users.begin() + index); //eraese from the connected users
	lck.unlock();
}

/*
Updates to the file every time there is an update
*/
void Server::writeToFile()
{
	while (true) 
	{
		unique_lock<mutex> lck(_muFileCon);
		_condFileWrite.wait(lck);
		if (_dataFile.is_open())
		{
			_dataFile.close();
		}
		_dataFile.open("data.txt", ios::out | ios::trunc); //clears the file for new content
		_dataFile << _fileContent;
		_dataFile.close();
		lck.unlock();
	}
}